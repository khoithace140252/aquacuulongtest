<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest1()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function testBasicTest2()
    {
        $response = $this->get('/san-pham/clock-fan-hien-thi-anh-nhieu-mau-sac_32.html');

        $response->assertStatus(200);
    }

    public function testBasicTest3()
    {
        $response = $this->get('/shop/qua-tang-cong-nghe_9.html');

        $response->assertStatus(200);
    }

    public function testBasicTest4()
    {
        $response = $this->get('/san-pham.html');

        $response->assertStatus(200);
    }

    public function testBasicTest5()
    {
        $response = $this->get('/shop/arduino-module_3.html');

        $response->assertStatus(200);
    }
}
